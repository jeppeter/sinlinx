#/bin/bash 

TOP_DIR=`pwd`

CROSS_COMPILE=$TOP_DIR/tools/external-toolchain/bin/arm-linux-gnueabi-    
UBOOT_DIR=$TOP_DIR/u-boot-2011.09
KERNEL_DIR=$TOP_DIR/linux-3.4
BUILDROOT_DIR=$TOP_DIR/buildroot
BUILDROOT_OUT_DIR=$TOP_DIR/buildroot/out
ROOTFS_DIR=$TOP_DIR/rootfs
HOSTTOOLS_DIR=$TOP_DIR/tools/bin
PACKTOOLS_DIR=$TOP_DIR/tools/pack
ROOTFS_OVERRIDE_DIR=$TOP_DIR/prebuilt/rootfs-override
APP_DIR=$TOP_DIR/app
BR_CROSS_COMPILE=$BUILDROOT_OUT_DIR/host/usr/bin/arm-buildroot-linux-gnueabihf-

copy_file_list=(
    $ROOTFS_OVERRIDE_DIR/etc/inittab:$ROOTFS_DIR/etc/inittab
    $ROOTFS_OVERRIDE_DIR/etc/profile:$ROOTFS_DIR/etc/profile
    $ROOTFS_OVERRIDE_DIR/etc/wpa_supplicant.conf:$ROOTFS_DIR/etc/wpa_supplicant.conf
    $ROOTFS_OVERRIDE_DIR/etc/hostapd.conf:$ROOTFS_DIR/etc/hostapd.conf
    $ROOTFS_OVERRIDE_DIR/etc/udhcpd.conf:$ROOTFS_DIR/etc/udhcpd.conf
    $ROOTFS_OVERRIDE_DIR/etc/mdev.conf:$ROOTFS_DIR/etc/mdev.conf
    $ROOTFS_OVERRIDE_DIR/etc/ts.conf:$ROOTFS_DIR/etc/ts.conf
    $ROOTFS_OVERRIDE_DIR/etc/init.d/rcS:$ROOTFS_DIR/etc/init.d/
    $ROOTFS_OVERRIDE_DIR/etc/init.d/S50sshd:$ROOTFS_DIR/etc/init.d/
    $ROOTFS_OVERRIDE_DIR/etc/init.d/S90camera:$ROOTFS_DIR/etc/init.d/
    $ROOTFS_OVERRIDE_DIR/etc/init.d/S90wifiap:$ROOTFS_DIR/etc/init.d/_S90wifiap
    $ROOTFS_OVERRIDE_DIR/etc/init.d/S90wifista:$ROOTFS_DIR/etc/init.d/_S90wifista
    $ROOTFS_OVERRIDE_DIR/etc/init.d/S99appstart:$ROOTFS_DIR/etc/init.d/
    $ROOTFS_OVERRIDE_DIR/etc/ssh/sshd_config:$ROOTFS_DIR/etc/ssh/
    $ROOTFS_OVERRIDE_DIR/root/*.sh:$ROOTFS_DIR/root/
    $ROOTFS_OVERRIDE_DIR/root/demo-h264enc:$ROOTFS_DIR/usr/bin/
    $APP_DIR/demo-camera/demo-camera:$ROOTFS_DIR/usr/bin/
    $APP_DIR/demo-qt/digitalclock:$ROOTFS_DIR/root/
    $PREBUILT_DIR/libs/*:$ROOTFS_DIR/lib/
)

function copy_file_to_rootfs()
{
  for line in ${copy_file_list[@]} ; do
	  srcfile=`echo $line | awk -F: '{print $1}'`
	  dstfile=`echo $line | awk -F: '{print $2}'`
	  cp -drf $srcfile $dstfile 2>/dev/null
  done
}

function build_uboot()
{
	local _force=$1

	if [ -z "$_force" ]
	then
		_force=0
	fi

	cd $UBOOT_DIR
	
	if [ $_force -gt 0 ] || [ ! -f $UBOOT_DIR/uboot_compile.timestamp ]
	then
		make -j8 CROSS_COMPILE=${CROSS_COMPILE} sun8iw8p1_spinand_emmc && \
		make -j8 CROSS_COMPILE=${CROSS_COMPILE} fes && \
	  make -j8 CROSS_COMPILE=${CROSS_COMPILE} boot0 && date > $UBOOT_DIR/uboot_compile.timestamp || exit 4
  	fi 
  [ $? -ne 0 ] && echo "build u-boot Failed"
}

function clean_uboot()
{
	cd $UBOOT_DIR
	make CROSS_COMPILE=${CROSS_COMPILE} distclean
	rm -f $UBOOT_DIR/uboot_compile.timestamp
}

function build_kernel()
{
	local _force=$1
	if [ -z "$_force" ]
	then
		_force=0
	fi

	cd $KERNEL_DIR
	
	if [ $_force -gt 0 ]  || [ ! -f $KERNEL_DIR/kernel_compile.timestamp ]
	then
		make ARCH=arm -j8 CROSS_COMPILE=${CROSS_COMPILE} sinlinx_defconfig && \
		make ARCH=arm -j8 CROSS_COMPILE=${CROSS_COMPILE} uImage modules && \
		make ARCH=arm CROSS_COMPILE=${CROSS_COMPILE} INSTALL_MOD_PATH=${ROOTFS_DIR} modules_install && \
		date > $KERNEL_DIR/kernel_compile.timestamp || exit 4
	fi
  [ $? -ne 0 ] && echo "build kernel Failed"
}

function clean_kernel()
{
	cd $KERNEL_DIR
	make ARCH=arm -j8 CROSS_COMPILE=${CROSS_COMPILE} distclean
	rm -f $KERNEL_DIR/kernel_compile.timestamp
}

function build_buildroot()
{
	echo "Build buildroot"

	local _force=$1

	if [ -z "$_force" ]
	then
		_force=0
	fi

	cd $BUILDROOT_DIR
	if [ -d $BUILDROOT_OUT_DIR ]
	then
		mkdir -p $BUILDROOT_OUT_DIR
	fi

	if [ $_force -gt 0 ] || [ ! -f $BUILDROOT_OUT_DIR/config_load.timestamp ]
	then
		cp configs/sinlinx_defconfig $BUILDROOT_OUT_DIR/.config
		make O=$BUILDROOT_OUT_DIR oldconfig && date >$BUILDROOT_OUT_DIR/config_load.timestamp || exit 4
	fi
	
	if [ $_force -gt 0 ] || [ ! -f $BUILDROOT_OUT_DIR/compile_complete.timestamp ]
	then
		make O=$BUILDROOT_OUT_DIR && date >$BUILDROOT_OUT_DIR/compile_complete.timestamp || exit 4
	fi

    rm -rf $ROOTFS_DIR
	mkdir -p $ROOTFS_DIR
	tar xvf $BUILDROOT_OUT_DIR/images/rootfs.tar.bz2 -C $ROOTFS_DIR
}

function clean_buildroot()
{
	cd $BUILDROOT_DIR

	make O=$BUILDROOT_OUT_DIR clean
 	
 	rm -f $BUILDROOT_OUT_DIR/compile_complete.timestamp
 	rm -f $BUILDROOT_OUT_DIR/config_load.timestamp
	rm -r $TOP_DIR/rootfs
}

function pack()
{

	copy_file_to_rootfs
	
	$HOSTTOOLS_DIR/make_ext4fs -s -l 90M /tmp/rootfs-ext4.img $ROOTFS_DIR
	
	cd $PACKTOOLS_DIR
	
	./pack -c sun8iw8p1 -p camdroid -b tiger-spinand-standard -e spinand
	
}

function build_demos()
{
	local _force=$1

	if [ -z "$_force" ]
	then
		_force=0
	fi

	cd $APP_DIR/demo-camera/
	if [ $_force -gt 0 ] || [ ! -f $APP_DIR/app_compile.timestamp ]
	then
		make CROSS_COMPILE=$BR_CROSS_COMPILE -j 6 demo-camera && cd $APP_DIR/demo-qt && \
		$TOP_DIR/buildroot/out/host/bin/qmake && make && date >$APP_DIR/app_compile.timestamp || exit 4
	fi
}

function clean_demos()
{
	cd $TOP_DIR
	
	cd $APP_DIR/demo-camera/
	make clean
	
	cd $APP_DIR/demo-qt
	make clean
	rm -f $APP_DIR/app_compile.timestamp
}

if [ $# -eq 0 ] ; then
	build_buildroot
	build_uboot
	build_kernel
	build_demos
	pack
else
	case $1 in
	clean)
		clean_demos
		clean_uboot
		clean_kernel
		clean_buildroot
		;;
	uboot)
		build_uboot "$2"
		;;
	kernel)
		build_kernel "$2"
		;;
	buildroot)
		build_buildroot "$2"
		;;
	app)
		build_demos "$2"
		;;
	pack)
		pack
		;;
	esac
fi
