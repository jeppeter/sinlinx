#!/bin/sh

# you should run the following command line to source this file
#
# source /root/qtenv.sh
#

export TSLIB_TSEVENTTYPE=H3600
export TSLIB_CONSOLEDEVICE=none
export TSLIB_FBDEVICE=/dev/fb0
export TSLIB_CALIBFILE=/etc/pointercal
export TSLIB_CONFFILE=/etc/ts.conf
export TSLIB_PLUGINDIR=/usr/lib/ts

export TSLIB_TSDEVICE=/dev/input/event2
export QWS_MOUSE_PROTO=tslib:$TSLIB_TSDEVICE

#export QWS_KEYBOARD=USB:/dev/input/event2
#export QWS_MOUSE_PROTO=USB:/dev/input/event1

